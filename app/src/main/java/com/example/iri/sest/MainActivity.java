package com.example.iri.sest;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.util.Calendar;

import static android.widget.Toast.LENGTH_LONG;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentGame = new Intent(getBaseContext(), FullscreenActivity.class);
                intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intentGame);

//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        TextView text = (TextView) findViewById(R.id.mainText);
        Calendar today = Calendar.getInstance();
        today.setTimeInMillis(System.currentTimeMillis());
        Calendar valid = Calendar.getInstance();
        valid.set(2016, 1, 11); //mesic od nuly!!!
        if(!valid.after(today)){
            Toast.makeText(getBaseContext(), "Přečti si znovu popis!!!", LENGTH_LONG).show();
            text.setText("8)\n" +
                    "V pátek v podvečer,\n" +
                    "bych se s Tebou moc rád sešel.\n" +
                    "V den půlročního výročí,\n" +
                    "chci Ti koukat do očí.\n" +
                    "\n\n\n\n\n" +
                    "\n\n\n\n\n" +
                    "pro více informací a 9. sloku pokračuj zde:\n\n" +
                    "http://znoj.8u.cz/danielka/");
        }
        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intentGame;
        Intent intentSkore;
        Intent intentVymazSkore;
        Intent intentVyberKolo;
        Intent intentAbout;

        if (id == R.id.nav_game) {
            intentGame = new Intent(this, FullscreenActivity.class);
            intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intentGame);
        } else if (id == R.id.nav_skore) {
            intentSkore = new Intent(this, Skore.class);
            intentSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            startActivity(intentSkore);
        } else if (id == R.id.nav_delete_skore) {
            vymazatSkore();
//            intentVymazSkore = new Intent(this, VymazatSkore.class);
//            intentVymazSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentVymazSkore);
        } else if (id == R.id.nav_vyber_kolo) {
            vybratKolo();
//            intentVyberKolo = new Intent(this, VybratKolo.class);
//            intentVyberKolo.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentVyberKolo);
        } else if (id == R.id.nav_about) {
            about();
//
//            intentAbout = new Intent(this, Info.class);
//            intentAbout.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentAbout);
        } else if (id == R.id.nav_exit) {
            finish();
            System.exit(0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void about() {
        AppCompatDialog d = new AppCompatDialog(this);
        d.setContentView(R.layout.info_layout);
        d.setTitle("Info o aplikaci");
        TextView tv = (TextView) d.findViewById(R.id.text);
        tv.setText("Šest měsíců\n" +
                "Rok: 2013, 2016\n" +
                "Autor programu: Jiří Znoj\n");
        d.show();
    }

    private void vymazatSkore() {
        AppCompatDialog d = new AppCompatDialog(this);
        d.setTitle("Opravdu vymazat skóre?");
        d.setContentView(R.layout.vymazat_layout);
        addListenerOnButtonVymazatScore(d);
        d.show();
    }

    private void addListenerOnButtonVymazatScore(final AppCompatDialog d) {
        Button button = (Button) d.findViewById(R.id.buttonAno);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                eraseAll();
                Toast.makeText(getApplicationContext(), "Všechny výsledky byly vymazány", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
            }
        });
        Button button2 = (Button) d.findViewById(R.id.buttonNe);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                d.cancel();
            }
        });

    }

    private void eraseAll() {
        TableDataSource db = new TableDataSource(this, true);
        try {
            db.eraseAll();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void vybratKolo() {
        TableDataSource db;
        NumberPicker np;

        AppCompatDialog d = new AppCompatDialog(this);
        d.setTitle("Vybrat kolo");
        d.setContentView(R.layout.vybrat_layout);
        db = new TableDataSource(this, true);

        np = (NumberPicker) d.findViewById(R.id.numberPicker);

        try {
            if(db.getLast() != null){
                np.setMaxValue((int)db.getLast().getId());
            }
            else{
                np.setMaxValue(1);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        np.setMinValue(1);

        addListenerOnButtonVybratKolo(d, np, db);
        d.show();
    }

    private void addListenerOnButtonVybratKolo(Dialog d, final NumberPicker np, final TableDataSource db) {
        //final Context context = this;
        Button button;
        Button button2;

        button = (Button) d.findViewById(R.id.level);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                int value;
                value = np.getValue();
                Log.w("?", "val: " + value);
                db.alterTable(0, value);

                Log.w("?", "val: "+value);
                Toast.makeText(getApplicationContext(), "Vybráno " + value + ". kolo", Toast.LENGTH_LONG).show();
            }
        });

        button2 = (Button) d.findViewById(R.id.play);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                int value;
                value = np.getValue();
                Log.w("?", "val: " + value);
                db.alterTable(0, value);

                Log.w("?", "val: "+value);
                Toast.makeText(getApplicationContext(), "Vybráno " + value + ". kolo", Toast.LENGTH_LONG).show();
                Intent intentGame = new Intent(getBaseContext(), FullscreenActivity.class);
                intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intentGame);

            }
        });
    }
}

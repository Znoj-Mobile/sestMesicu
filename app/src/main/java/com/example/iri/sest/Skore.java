package com.example.iri.sest;
import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;

public class Skore extends ListActivity {
	private TableDataSource db;
	
	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        db = new TableDataSource(this, false);
	    db.open();
        populateList();
    }
	
	private void populateList() {
		this.db = new TableDataSource(this, true);
		String[] records = this.db.selectScore();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, records);
            setListAdapter(adapter);
	}
	
}

package com.example.iri.sest;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class TableDataSource {
	private SQLiteDatabase database;
	private SQLiteStatement insertStmt;
	private SQLiteStatement updateStmt;
	private MySQLiteHelper dbHelper;
	private static final String allColumns = (
			MySQLiteHelper.COLUMN_ID + ", " +
			MySQLiteHelper.COLUMN_VALUE );
	
	private static final String INSERT = "insert into " + MySQLiteHelper.TABLE + "("
								+ allColumns + ") values (?, ?)";
	private static final String SELECT = "select " + allColumns + " from " + MySQLiteHelper.TABLE;
	private static final String UPDATE = "update " + MySQLiteHelper.TABLE + " set " + MySQLiteHelper.COLUMN_VALUE + 
										"= ?" + "where " + MySQLiteHelper.COLUMN_ID + "= ?"; 
	private Cursor c;
	
	
	public Table fromCursor(Cursor cursor) throws ParseException {
		return new Table(cursor.getLong(0), c.getInt(1));
	}
	
	public Table getFirst() throws ParseException {
		c = this.database.rawQuery(SELECT, null);
		if (c.moveToFirst()) {
			return fromCursor(c);
		} 
		else{
			Log.w("MOJE", "getFirstNull");
			//vlozeni radku a zavolani teto funkce
			this.insert(new Table(0,1));
			
			for(int i = 1; i<=Game.pocet_kol+1; i++){
				this.insert(new Table(i,999));
			}

			return this.getFirst();
		}
			
	}
	
	
	public Table getLast() throws ParseException {
		c = this.database.rawQuery(SELECT, null);
		if (c.moveToLast()) {
			return fromCursor(c);
		}
		return null;	
	}
	
	public TableDataSource(Context context, boolean readOnly) {
		dbHelper = new MySQLiteHelper(context);
		this.database = readOnly ? dbHelper.getReadableDatabase() : dbHelper.getWritableDatabase();
		this.insertStmt = this.database.compileStatement(INSERT);
		this.updateStmt = this.database.compileStatement(UPDATE);
		Log.w("MOJE", "constr.3");
    }
	
	public void open() throws SQLException {
	    //database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
	    database.close();
	}
	
	public long alterTable(int level, int value){
		//if(level == 0){
			//ContentValues updateCurrent = new ContentValues();
			//updateCurrent.put("current_level", String.valueOf(value));
			//database.update("table1", updateCurrent, "_id=1", allColumns);
		Log.w("UPDATE", ""+value);
		this.updateStmt.bindLong(2, level);
		this.updateStmt.bindLong(1, value);
		//}
		return this.updateStmt.executeInsert();
	}
	
	public void eraseAll() throws ParseException{
		try {
			if(this.getLast() != null){
				int posledniID = (int) this.getLast().getId();
				for(int i = 1; i <= posledniID; i++){
					alterTable(i, 999);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Table getBest(int level) throws ParseException {
		
		try{
			//Log.w("getBest", "sel");
			c = database.rawQuery(SELECT, null);
		} catch (Exception e){
			e.getStackTrace();
		}
		//Log.w("getBest", "c.moveToFirst");
		if (c.moveToFirst()) {
			do {
				if(c.getLong(0) == level){
					Log.w("getBest", ""+c.getInt(1));
					return fromCursor(c);
				}
				Log.w("getBest", "next");
			} while (c.moveToNext());
		}
		if (c != null && !c.isClosed()) {
			c.close();
		}
		
		return null;
	}

	public long insert(Table table) {
		Log.w("MOJE", "insertetd1");
		this.insertStmt.bindLong(1, table.getId());
		Log.w("MOJE", "insertetd2");
		this.insertStmt.bindLong(2, table.getValue());
		return this.insertStmt.executeInsert();
	}

	public String[] selectScore() {
		try{
			//Log.w("selectScore", "sel");
			c = database.rawQuery(SELECT, null);
		} catch (Exception e){
			e.getStackTrace();
		}
		//Log.w("selectScore", "c.moveToFirst");
		List<String> list = new ArrayList<String>();
		list.add("Kolo \t Nejlepší skóre");
		String s = "";
		int _id = 0;
		if (c.moveToFirst()) {
			do {
				try {
					 _id = (int) fromCursor(c).getId();
					if(_id > 0){
						if(fromCursor(c).getValue() == 999){
							s = String.valueOf(fromCursor(c).getId()) + " \t\t\t\t -";
						}
						else{
							 s = fromCursor(c).getId() + " \t\t\t\t " + String.valueOf(fromCursor(c).getValue());
						}
						list.add(s);
					}
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} while (c.moveToNext());
		}
		if (c != null && !c.isClosed()) {
			c.close();
		}
		String[] stringArr = new String[list.size()];
		stringArr = list.toArray(stringArr);
		
		return stringArr;
	}
}

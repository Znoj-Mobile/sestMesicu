package com.example.iri.sest;

public class Table {
	private long _id;
	private int value;
	
	public Table(long id, int val){
		this._id = id;
		this.value = val;
	}
	public Table() {
		// TODO Auto-generated constructor stub
	}
	public long getId() {
		return _id;
	}
	public void setId(long id) {
		this._id = id;
	}
	
	public int getValue() {
		return this.value;
	}
	public void setValue(int val) {
		this.value = val;
	}
	
}

package com.example.iri.sest;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;

/**
 * Created by Iri on 14.01.2016.
 */
@SuppressLint("ShowToast")
public class FullscreenActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Game game = null;
    int aktualni_kolo;
    int skore_kola = 0;
    int kroky = 0;
    private TableDataSource datasource;

    TextView vl = null;
    TextView vr = null;
    TextView vrr = null;


    Table score = new Table();

    private void aktualizace(){
        int kolo = game.getKola();
        //pro skore si ukladam kroky
        if(game.getKroky() != 0){
            kroky = game.getKroky();
        }
        vl.setText("Kolo: " + kolo);
        vr.setText(", Kroků: " + game.getKroky());
        if(skore_kola != 999){
            vrr.setText(", Nej: " + skore_kola);
        }
        else{
            vrr.setText(", Nej: " + "-");
        }
        if(game.cil){
            if((skore_kola == 0) || (skore_kola > kroky)){
                Log.w("skore ON CLICK", ""+skore_kola);
                Toast.makeText(getApplicationContext(), "Překonáno nejlepší skóre tohoto kola", Toast.LENGTH_SHORT).show();
                datasource.alterTable(game.getKola()-1, kroky);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen);

//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, null, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        datasource = new TableDataSource(this, false);
        datasource.open();

        Table t = new Table();

        try {
            t = datasource.getFirst();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        game = (Game) this.findViewById(R.id.Game);

        aktualni_kolo = (int) t.getValue();
        try {
            score = datasource.getBest(aktualni_kolo);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        skore_kola = score.getValue();
        Log.w("skore", ""+skore_kola);
        game.setKolo(aktualni_kolo);

        vl = (TextView) findViewById(R.id.textView1);
        vr = (TextView) findViewById(R.id.textView2);
        vrr = (TextView) findViewById(R.id.textView3);

        aktualizace();

        ((Button) findViewById(R.id.buttonL)).setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ShowToast")
            public void onClick(View view) {
                if(game.onKeyDown(Directions.Left)){
                    aktualizace();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Pro pokračování klikni na obrázek", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((Button) findViewById(R.id.buttonR)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(game.onKeyDown(Directions.Right)){
                    aktualizace();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Pro pokračování klikni na obrázek", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((Button) findViewById(R.id.buttonU)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(game.onKeyDown(Directions.Up)){
                    aktualizace();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Pro pokračování klikni na obrázek", Toast.LENGTH_SHORT).show();
                }
            }
        });
        ((Button) findViewById(R.id.buttonD)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if(game.onKeyDown(Directions.Down)){
                    aktualizace();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Pro pokračování klikni na obrázek", Toast.LENGTH_SHORT).show();
                }
            }
        });

        game.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                game.obrazek_kola();

                if(game.GetKonec()){
                    game.SetKonec();
                    Toast.makeText(getApplicationContext(), "Poslední kolo dokončeno.", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                    datasource.alterTable(0, 1);
                    game.setLastKolo(1);
                    return;
                }

                int kolo = game.getKola();

                Log.w("skore-game", ""+ kroky);

                try {
                    score = datasource.getBest(game.getKola());
                    skore_kola = score.getValue();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                aktualizace();
                Log.w("0", ""+kolo);
                datasource.alterTable(0, kolo);
            }
        });

    }

    //menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }




    //odchytavani menu
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        Table t = new Table();
        try {
            t = datasource.getFirst();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        switch (item.getItemId()) {
            case R.id.restart:
                game.novy_level();
                aktualizace();
                return true;
            case R.id.next:
                game.next_lap();
                if(game.GetKonec()){
                    game.SetKonec();
                    Toast.makeText(getApplicationContext(), "Dokončil jsi poslední kolo", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getBaseContext(), MainActivity.class);
                    startActivity(intent);
                    datasource.alterTable(0, 1);
                    game.setLastKolo(1);
                    return true;
                }
                else{
                    datasource.alterTable(0, game.getKola());
                    try {
                        score = datasource.getBest(game.getKola());
                        skore_kola = score.getValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    aktualizace();
                }
                return true;
            case R.id.reset:
                game.reset_lap();
                datasource.alterTable(0, game.getKola());
                try {
                    score = datasource.getBest(1);
                    skore_kola = score.getValue();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                aktualizace();
                return true;
            case R.id.menu:
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
            default:
                return super.onOptionsItemSelected(item);
        }
    }


	/*
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);

		// Checks the orientation of the screen
		if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
		    Toast.makeText(this, "landscape", Toast.LENGTH_SHORT).show();
		    setContentView(R.layout.activity_fullscreen_landscape);
		} else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
		    Toast.makeText(this, "portrait", Toast.LENGTH_SHORT).show();
		    setContentView(R.layout.activity_fullscreen);
		}

	    game = (Game) this.findViewById(R.id.Game);
		((Button) findViewById(R.id.buttonL)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                game.onKeyDown(3);
            }
        });
		((Button) findViewById(R.id.buttonR)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                game.onKeyDown(2);
            }
        });
		((Button) findViewById(R.id.buttonU)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                game.onKeyDown(0);
            }
        });
		((Button) findViewById(R.id.buttonD)).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                game.onKeyDown(1);
            }
        });

	}
	*/

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intentGame;
        Intent intentSkore;
        Intent intentVymazSkore;
        Intent intentVyberKolo;
        Intent intentAbout;
        if(id == R.id.restart){
            game.novy_level();
            aktualizace();
        }
        else if(id == R.id.next){
            game.next_lap();
            if(game.GetKonec()){
                game.SetKonec();
                Toast.makeText(getApplicationContext(), "Poslední kolo dokončeno", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getBaseContext(), MainActivity.class);
                startActivity(intent);
                datasource.alterTable(0, 1);
                game.setLastKolo(1);
            }
            else{
                datasource.alterTable(0, game.getKola());
                try {
                    score = datasource.getBest(game.getKola());
                    skore_kola = score.getValue();
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                aktualizace();
            }
        }
        else if(id == R.id.reset){
            game.reset_lap();
            datasource.alterTable(0, game.getKola());
            try {
                score = datasource.getBest(1);
                skore_kola = score.getValue();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            aktualizace();
        }
        else if(id == R.id.menu){
            Intent intent = new Intent(getBaseContext(), MainActivity.class);
            startActivity(intent);
        }

//        else if (id == R.id.nav_game) {
////            intentGame = new Intent(this, FullscreenActivity.class);
////            intentGame.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
////            startActivity(intentGame);
//        } else if (id == R.id.nav_skore) {
//            intentSkore = new Intent(this, Skore.class);
//            intentSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentSkore);
//
//        } else if (id == R.id.nav_delete_skore) {
//            intentVymazSkore = new Intent(this, VymazatSkore.class);
//            intentVymazSkore.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentVymazSkore);
//
//
//        } else if (id == R.id.nav_vyber_kolo) {
//            intentVyberKolo = new Intent(this, VybratKolo.class);
//            intentVyberKolo.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentVyberKolo);
//
//        } else if (id == R.id.nav_about) {
//            intentAbout = new Intent(this, Info.class);
//            intentAbout.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
//            startActivity(intentAbout);
//
//        } else if (id == R.id.nav_exit) {
//            finish();
//            System.exit(0);
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}

package com.example.iri.sest;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.util.Calendar;

import static android.widget.Toast.LENGTH_LONG;

public class Game extends View{
	boolean mShowText;
	int mTextPos;
	private int heroX = 7;
    private int heroY = 1;
	private Directions last_direction;

    private int sirkaDisp;
    private int sirkaPic;
    public boolean cil = false;
    private int kolo = 0;
    private int kroku = 0;
    private boolean konec = false;
	private boolean notProperTime = false;
    //pocet kol = pocitano od 0
    public static int pocet_kol = 0;
    

    
    //private TextView vr = (TextView) findViewById(R.id.textView2);
    
    private int[] level = new int[101];

    Bitmap[] bmp;
    Bitmap bBox, bBoxOK, bWall, bHero, bSpace, bGoal;
    
	public Game(Context context, AttributeSet attrs) {
		   super(context, attrs);
		   setFocusable(true);
           setFocusableInTouchMode(true);
           
           bmp = new Bitmap[12];

           Field[] fields = R.raw.class.getFields();
   		   pocet_kol = fields.length - 1;

           bmp[0] = BitmapFactory.decodeResource(getResources(), R.drawable.empty);
           bmp[1] = BitmapFactory.decodeResource(getResources(), R.drawable.wall);
           bmp[2] = BitmapFactory.decodeResource(getResources(), R.drawable.box);
           bmp[3] = BitmapFactory.decodeResource(getResources(), R.drawable.hole);
           bmp[4] = BitmapFactory.decodeResource(getResources(), R.drawable.hero);
           bmp[5] = BitmapFactory.decodeResource(getResources(), R.drawable.boxok);
           bmp[6] = BitmapFactory.decodeResource(getResources(), R.drawable.goal);
           bmp[7] = BitmapFactory.decodeResource(getResources(), R.drawable.finish);
           bmp[8] = BitmapFactory.decodeResource(getResources(), R.drawable.game_end);

			bmp[9] = BitmapFactory.decodeResource(getResources(), R.drawable.hero_up);
			bmp[10] = BitmapFactory.decodeResource(getResources(), R.drawable.hero_down);
			bmp[11] = BitmapFactory.decodeResource(getResources(), R.drawable.hero_back);
           
           //vyska = sirka
           sirkaPic = bmp[0].getHeight();
           
           novy_level();
           
           
           
           TypedArray a = context.getTheme().obtainStyledAttributes(
		        attrs, R.styleable.Game, 0, 0);

		   try {
		       mShowText = a.getBoolean(R.styleable.Game_showText2, false);
		       mTextPos = a.getInteger(R.styleable.Game_labelPosition, 0);
		   } finally {
		       a.recycle();
		   }
	}
	
	public int getKola(){
		return kolo+1;
	}
	public void setKolo(int kolo_db){
		kolo = kolo_db - 1;
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(System.currentTimeMillis());
		Calendar valid = Calendar.getInstance();
		valid.set(2016, 0, 30); //mesic od nuly!!!
		if( kolo >= 5 && valid.after(today)){
			Toast.makeText(getContext(), "na 6. kolo ještě musíš počkat do 30. 1. 2016", LENGTH_LONG).show();
			notProperTime = true;
		}
		valid.set(2016, 1, 1);
		if( kolo >= 9 && valid.after(today)){
			Toast.makeText(getContext(), "na 10. kolo ještě musíš počkat do 1. 2. 2016", LENGTH_LONG).show();
			notProperTime = true;
		}
		if(notProperTime){
			setLastKolo(kolo - 1);
			notProperTime = false;
			Intent intent = new Intent(getContext(), MainActivity.class);
			getContext().startActivity(intent);
			return;
		}
		novy_level();
	}
	public void setLastKolo(int kolo_db){
		kolo = kolo_db - 1;
	}
	public int getKroky(){
		return kroku;
	}
	public void novy_level(){
		last_direction = Directions.Left;
		heroX = 7;
	    heroY = 1;
	    kroku = 0;
	    
	    String mapa = "mapa" + kolo;
		InputStream is = null;
		Calendar today = Calendar.getInstance();
		today.setTimeInMillis(System.currentTimeMillis());
		Calendar valid = Calendar.getInstance();
		valid.set(2016, 0, 30); //mesic od nuly!!!
		if( kolo > 5 && valid.after(today)){
			Toast.makeText(getContext(), "na 6. kolo ještě musíš počkat do 30. 1. 2016", LENGTH_LONG).show();
			notProperTime = true;
		}
		valid.set(2016, 1, 1);
		if( kolo > 9 && valid.after(today)){
			Toast.makeText(getContext(), "na 10. kolo ještě musíš počkat do 1. 2. 2016", LENGTH_LONG).show();
			notProperTime = true;
		}

		if(notProperTime){
			setLastKolo(kolo - 1);
			notProperTime = false;
			Intent intent = new Intent(getContext(), MainActivity.class);
			getContext().startActivity(intent);
			return;
		}
		notProperTime = false;
		if(kolo > pocet_kol){
			konec = true;
			return;
	    }
		else{
			Context c = getContext();
			int id = getResources().getIdentifier(mapa, "raw", c.getPackageName());
			is = getResources().openRawResource(id); 
		}
		
        BufferedReader br = new BufferedReader(new InputStreamReader(is));
        String readLine = null;

        int counter = 0;
        try {
        	String[] array;
            while ((readLine = br.readLine()) != null) {
         	   array = readLine.split(",");
         	   for(int i = 0; i < array.length; i++){
         		//Log.d("MOJE","Code =" + array[i] + " i: " + i + " counter: " + counter);
         		level[counter] = Integer.parseInt(array[i]);
         	   	counter += 1;
         	   }

            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //prekresli
        invalidate();
	}
	
	
	public boolean onKeyDown(Directions dir) {
		last_direction = dir;
		//pokud neni zobrazeno herni pole
		if(this.cil){
			return false;
		}
        //Log.d("MOJE","keyCode =" + keyCode + " heroX: " + heroX + "heroY: " + heroY);
        if(dir == Directions.Up){
	    	if(level[(heroY-1)*10+heroX] == 0){
	        	heroY = heroY - 1;
	        	kroku += 1;
	    	}
	    	//cil
	    	else if(level[(heroY-1)*10+heroX] == 6){
	    		heroY = heroY - 1;
	    		kroku += 1;
	    	}
	    	//pokud je nad hrdinou krabice
	    	else if(level[(heroY-1)*10+heroX] == 2){
	    			//pokud je nad krabici dira
	    			if(level[(heroY-2)*10+heroX] == 3){
	    				//krabici presun a obarvi na zeleno
	    				level[(heroY-2)*10+heroX] = 5;
	    				level[(heroY-1)*10+heroX] = 0;
	    				heroY = heroY - 1;
	    				kroku += 1;
	    			}
	    			//pokud je nad krabici prazdny misto
	    			else if(level[(heroY-2)*10+heroX] == 0){
	    				//presun krabici
	    				level[(heroY-2)*10+heroX] = 2;
	    				level[(heroY-1)*10+heroX] = 0;
	    				heroY = heroY - 1;
	    				kroku += 1;
	    			}
	    			//pokud je nad krabici zelena krabice
	    			else if(level[(heroY-2)*10+heroX] == 5){
	    				//presun krabici
	    				level[(heroY-2)*10+heroX] = 2;
	    				level[(heroY-1)*10+heroX] = 0;
	    				heroY = heroY - 1;
	    				kroku += 1;
	    			}
	    			
	    	}
	    	//pokud je nad hrdinou zelena krabice
	    	else if(level[(heroY-1)*10+heroX] == 5){
	    		heroY = heroY - 1;
	    		kroku += 1;
	    	}
	    }
	    if(dir == Directions.Down){
	    	if(level[(heroY+1)*10+heroX] == 0){
	        	heroY = heroY + 1;
	        	kroku += 1;
	    	}
	    	//cil
	    	else if(level[(heroY+1)*10+heroX] == 6){
	    		heroY = heroY + 1;
	    		kroku += 1;
	    	}
	    	//krabice
	    	else if(level[(heroY+1)*10+heroX] == 2){
    			//pod krabici dira
	    		if(level[(heroY+2)*10+heroX] == 3){
    				//posun krabici, prebarvi, misto na prazdno
	    			level[(heroY+2)*10+heroX] = 5;
    				level[(heroY+1)*10+heroX] = 0;
    				heroY = heroY + 1;
    				kroku += 1;
    			}
	    		//pod krabici prazdno
    			else if(level[(heroY+2)*10+heroX] == 0){
    				//posun krabici
    				level[(heroY+2)*10+heroX] = 2;
    				level[(heroY+1)*10+heroX] = 0;
    				heroY = heroY + 1;
    				kroku += 1;
    			}
	    		//pokud je pod krabici zelena krabice
    			else if(level[(heroY+2)*10+heroX] == 5){
    				//presun krabici
    				level[(heroY+2)*10+heroX] = 2;
    				level[(heroY+1)*10+heroX] = 0;
    				heroY = heroY + 1;
    				kroku += 1;
    			}
    			
	    	}
	    	//zelena krabice
	    	else if(level[(heroY+1)*10+heroX] == 5){
    			heroY = heroY + 1;
    			kroku += 1;
	    	}
	    }
	    if(dir == Directions.Right){
	    	if(level[heroY*10+heroX+1] == 0){
	    		heroX = heroX + 1;
	    		kroku += 1;
	    	}
	    	else if(level[heroY*10+heroX+1] == 6){
	    		heroX = heroX + 1;
	    		kroku += 1;
	    	}
	    	else if(level[heroY*10+heroX+1] == 2){
    			if(level[heroY*10+heroX+2] == 3){
    				level[heroY*10+heroX+2] = 5;
    				level[heroY*10+heroX+1] = 0;
    				heroX = heroX + 1;
    				kroku += 1;
    			}
    			else if(level[heroY*10+heroX+2] == 0){
    				level[heroY*10+heroX+2] = 2;
    				level[heroY*10+heroX+1] = 0;
    				heroX = heroX + 1;
    				kroku += 1;
    			}
    			else if(level[heroY*10+heroX+2] == 5){
    				level[heroY*10+heroX+2] = 2;
    				level[heroY*10+heroX+1] = 0;
    				heroX = heroX + 1;
    				kroku += 1;
    			}
    			
	    	}
	    	else if(level[heroY*10+heroX+1] == 5){
	    		heroX = heroX + 1;
	    		kroku += 1;
	    	}
	    }
	    if(dir == Directions.Left){
	    	if(level[heroY*10+heroX-1] == 0){
	        	heroX = heroX - 1;
	        	kroku += 1;
	    	}
	    	else if(level[heroY*10+heroX-1] == 6){
	        	heroX = heroX - 1;
	        	kroku += 1;
	    	}
	    	else if(level[heroY*10+heroX-1] == 2){
    			if(level[heroY*10+heroX-2] == 3){
    				level[heroY*10+heroX-2] = 5;
    				level[heroY*10+heroX-1] = 0;
    				heroX = heroX - 1;
    				kroku += 1;
    			}
    			else if(level[heroY*10+heroX-2] == 0){
    				level[heroY*10+heroX-2] = 2;
    				level[heroY*10+heroX-1] = 0;
    				heroX = heroX - 1;
    				kroku += 1;
    			}
    			else if(level[heroY*10+heroX-2] == 5){
    				level[heroY*10+heroX-2] = 2;
    				level[heroY*10+heroX-1] = 0;
    				heroX = heroX - 1;
    				kroku += 1;
    			}
	    	}
	    	else if(level[heroY*10+heroX-1] == 5){
	    		heroX = heroX - 1;
	    		kroku += 1;
	    	}
	    }
	    
	    if(level[heroY*10+heroX] == 6){
    		this.cil = true;
    		kolo += 1;
    		novy_level();
	    }
	    
	    //vr.setText(", Kroků: " + kroku);
	    
        invalidate();
        return true;
	}
	
	@SuppressLint("DrawAllocation")
	@Override
    protected void onDraw(Canvas canvas) {

            Paint tPaint = new Paint();
            int mezera = 0;
        	sirkaDisp = canvas.getWidth()/10;
        	if(sirkaPic != sirkaDisp){
        		mezera = (sirkaDisp-sirkaPic)*5;
        	}
        	if(this.konec){
        		//canvas.drawBitmap(bmp[8], mezera, 0, tPaint);
				try {
					canvas.drawBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.class.getField("game_end").getInt(null)), mezera, 0, tPaint);
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
				return;
        	}
        	else if(this.cil){
        		//canvas.drawBitmap(bmp[7], mezera, 0, tPaint);
				try {
					Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.class.getField("end" + kolo).getInt(null));
					canvas.drawBitmap(bitmap, mezera, 0, tPaint);
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
        		return;
        	}
        	else{
        		for (int i = 0; i < 10; i++)
                    for (int j = 0; j < 10; j++) {
						//pokud je vykreslovan cil, je nutne jej nejdrive podlozit podkladem
						if(level[i*10+j] == 6){
							canvas.drawBitmap(bmp[0], j*(sirkaPic)+mezera, i*(sirkaPic), tPaint);
						}
                    	if(i == 0){
                    		canvas.drawBitmap(bmp[level[i*10+j]], j*(sirkaPic)+mezera, i*(sirkaPic), tPaint);
                    	}
                    	else{
                    		canvas.drawBitmap(bmp[level[i*10+j]], j*(sirkaPic)+mezera, i*(sirkaPic), tPaint);
                    	}
                    }
        	}
			int direction = 4;
			if(last_direction == Directions.Up){
				direction = 9;
			}
			else if(last_direction == Directions.Down){
				direction = 10;
			}
			else if(last_direction == Directions.Right){
				direction = 11;
			}
            canvas.drawBitmap(bmp[direction], heroX*(sirkaPic)+mezera, heroY*(sirkaPic), tPaint);
            
    }  
	
	public void next_lap() {
		if(kolo >= Game.pocet_kol){
			konec = true;
			invalidate();
			return;
		}
		else{
			kolo += 1;
		}
		novy_level();
	}
	public void reset_lap() {
		kolo = 0;
		novy_level();
	}
	
	public void obrazek_kola(){
		if(this.cil){
			novy_level();
			this.cil = false;
		}
	}
	
	public void SetKonec(){
		konec = false;
	}
	
	public boolean GetKonec(){
		return this.konec;
	}

}
